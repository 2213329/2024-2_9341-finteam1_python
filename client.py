import atexit
import sys
from tkinter import Tk, messagebox
import Boggled
from UserLogin import LoginWindow
from MainMenu import MainMenu
from connector import Connector
from LeaderboardView import LeaderboardView
from Lobby import Lobby  # Import the Lobby class

class Client:
    def cleanup_before_exit(self):
        try:
            if self.playerData is not None:
                self.servant.playerLogout(self.playerData.username)
            sys.exit()
        except Boggled.AccountNotLoggedIn:
            sys.exit()
        except Exception as e:
            messagebox.showerror("Logout Error", f"Error during logout: {e}")
            sys.exit()

    def __init__(self):
        self.main_menu = None
        self.loggedIn = False
        self.playerData = None
        self.servant = None
        self.gameData = None

    def run(self):
        con = Connector()
        self.servant = con.servant
        atexit.register(self.cleanup_before_exit)
        self.window = Tk()
        self.show_login_window()
        self.window.mainloop()

    def show_login_window(self):
        self.login_window = LoginWindow(self.window, self)

    def show_main_menu(self):
        self.main_menu = MainMenu(self.window, self)

    def handle_view_leaderboard(self):
        self.show_leaderboard()

    def handle_start_game(self):
        self.show_lobby()

    def show_lobby(self):
        for widget in self.window.winfo_children():
            widget.destroy()
        Lobby(self.window, self, self.playerData)

    def show_leaderboard(self):
        try:
            leaderboard = self.servant.getLeaderboard()
            player_info = self.playerData
            self.window.title("Leaderboard")  # Update title for the main window
            LeaderboardView(self.window, self, leaderboard, player_info)
        except Exception as e:
            print("An error occurred while getting the leaderboard:", e)

    def login(self, username, password):
        try:
            credentials = Boggled.PlayerCredentials(username=username, password=password)
            self.playerData = self.servant.playerLogin(credentials)
            self.loggedIn = True
            messagebox.showinfo("Login Success", f"Successfully logged in as {username}")
            self.show_main_menu()
        except Boggled.InvalidCredentials:
            messagebox.showerror("Login Error", "Invalid credentials")
        except Boggled.AccountAlreadyLoggedIn:
            messagebox.showerror("Login Error", "Account already logged in")
        except Boggled.ServerOffline:
            messagebox.showerror("Login Error", "Server is offline")
        except Exception as e:
            messagebox.showerror("Login Error", f"An error occurred: {e}")

if __name__ == '__main__':
    client = Client()
    client.run()

from pathlib import Path
from tkinter import Tk, Canvas, Entry, Button, PhotoImage

class LoginWindow:
    def __init__(self, root, client):
        self.root = root
        self.client = client
        self.root.geometry("1077x620")
        self.root.configure(bg="#FFFFFF")

        self.canvas = Canvas(
            self.root,
            bg="#FFFFFF",
            height=620,
            width=1077,
            bd=0,
            highlightthickness=0,
            relief="ridge"
        )
        self.canvas.place(x=0, y=0)

        self.setup_ui()

    def relative_to_assets(self, path: str) -> Path:
        OUTPUT_PATH = Path(__file__).parent
        ASSETS_PATH = OUTPUT_PATH / Path(r"assets/login")
        return ASSETS_PATH / Path(path)

    def setup_ui(self):
        image_image_1 = PhotoImage(file=self.relative_to_assets("image_1.png"))
        self.canvas.create_image(538.0, 309.0, image=image_image_1)

        image_image_2 = PhotoImage(file=self.relative_to_assets("image_2.png"))
        self.canvas.create_image(538.526, 400.439, image=image_image_2)

        entry_image_1 = PhotoImage(file=self.relative_to_assets("entry_1.png"))
        entry_bg_1 = self.canvas.create_image(594.245, 352.755, image=entry_image_1)
        self.entry_1 = Entry(bd=0, bg="#FEF8F0", fg="#000716", highlightthickness=0)
        self.entry_1.place(x=527.357, y=333.461, width=133.777, height=36.587)

        entry_image_2 = PhotoImage(file=self.relative_to_assets("entry_2.png"))
        entry_bg_2 = self.canvas.create_image(594.245, 417.258, image=entry_image_2)
        self.entry_2 = Entry(bd=0, bg="#FEF8F0", fg="#000716", highlightthickness=0, show="*")
        self.entry_2.place(x=527.357, y=397.965, width=133.777, height=36.587)

        self.canvas.create_text(
            418.0,
            343.0,
            anchor="nw",
            text="Username",
            fill="#C29269",
            font=("Karla ExtraBold", 18 * -1)
        )

        self.canvas.create_text(
            416.143,
            407.756,
            anchor="nw",
            text="Password",
            fill="#C29269",
            font=("Karla ExtraBold", 18 * -1)
        )

        button_image_1 = PhotoImage(file=self.relative_to_assets("button_1.png"))
        self.button_1 = Button(
            image=button_image_1,
            borderwidth=0,
            highlightthickness=0,
            command=self.on_login,
            relief="flat"
        )
        self.button_1.place(x=408.0, y=464.772, width=262.617, height=38.587)

        image_image_3 = PhotoImage(file=self.relative_to_assets("image_3.png"))
        self.canvas.create_image(539.0, 233.0, image=image_image_3)

        self.root.resizable(False, False)
        # Keep a reference to the images to prevent garbage collection
        self.images = [image_image_1, image_image_2, entry_image_1, entry_image_2, button_image_1, image_image_3]

    def on_login(self):
        username = self.entry_1.get()
        password = self.entry_2.get()
        self.client.login(username, password)


if __name__ == "__main__":
    window = Tk()
    client = None  # Replace with actual Client instance if needed
    login_window = LoginWindow(window, client)
    window.mainloop()

import sys
import CORBA
import CosNaming
import Boggled
from configparser import ConfigParser

#ORBtraceLevel=25
class Connector:
    def __init__(self):
        self.read_config()
        self.orb = CORBA.ORB_init(self.args, CORBA.ORB_ID)
        self.obj = self.orb.resolve_initial_references("NameService")
        self.rootContext = self.obj._narrow(CosNaming.NamingContext)
        if self.rootContext is None:
            print("Failed to narrow the root naming context")
            sys.exit(1)
        self.name = [CosNaming.NameComponent("BoggledGameplay", "")]
        try:
            self.obj = self.rootContext.resolve(self.name)
        except CosNaming.NamingContext.NotFound as ex:
            print("Name not found")
            sys.exit(1)
        self.servant = self.obj._narrow(Boggled.Gameplay)


    def read_config(self):
        # read the configuration file
        config = ConfigParser()
        config.read('Config.ini')
        host = config.get('server', 'host')
        port = config.get('server', 'port')
        self.args = ['-ORBInitRef', f'NameService=corbaloc::{host}:{port}/NameService']

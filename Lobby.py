import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import threading
from pathlib import Path

import Boggled
from GameWindow import GameWindow  # Import the GameWindow class

class Lobby:
    def __init__(self, root, client, player_info):
        self.root = root
        self.client = client
        self.player_info = player_info
        self.game_info = None
        self.game_window_opened = False
        self.main_menu_opened = False
        self.left_lobby = False

        self.init_ui()
        self.join_game_async()

    def relative_to_assets(self, path: str) -> Path:
        OUTPUT_PATH = Path(__file__).parent
        ASSETS_PATH = OUTPUT_PATH / Path(r"assets/res")
        return ASSETS_PATH / Path(path)

    def init_ui(self):
        self.root.title("Lobby")
        self.root.geometry("1077x622")
        self.root.resizable(False, False)

        try:
            background_image = Image.open(self.relative_to_assets("lobby.png"))
            background_photo = ImageTk.PhotoImage(background_image)
            background_label = tk.Label(self.root, image=background_photo)
            background_label.image = background_photo  # keep a reference!
            background_label.place(x=0, y=0, relwidth=1, relheight=1)

            self.status_label = tk.Label(self.root, text="Waiting for game to start...", font=("Serif", 20, "bold"), fg="black", bg="white")
            self.status_label.place(x=0, y=250, width=self.root.winfo_width(), height=30)

            leave_button_image = Image.open(self.relative_to_assets("leave.png"))
            leave_button_photo = ImageTk.PhotoImage(leave_button_image)
            leave_button = tk.Button(self.root, image=leave_button_photo, command=self.handle_leave_game)
            leave_button.image = leave_button_photo  # keep a reference!
            leave_button.place(x=430, y=500, width=217, height=48)

        except Exception as e:
            print(e)

    def open_game_window_if_ready(self):
        if not self.game_window_opened and self.game_info:
            self.game_window_opened = True
            self.root.after(0, self.open_game_window)

    def open_game_window(self):
        self.root.destroy()  # Close the lobby window
        root = tk.Tk()
        GameWindow(root, self.client, self.player_info, self.game_info)
        root.mainloop()

    def open_main_menu_if_not_opened(self):
        if not self.main_menu_opened:
            self.main_menu_opened = True
            self.root.after(0, self.client.show_main_menu)

    def handle_leave_game(self):
        self.left_lobby = True
        try:
            self.client.servant.leaveGame(self.player_info.username)
            self.open_main_menu_if_not_opened()
        except Boggled.LobbyException as e:
            messagebox.showerror("Error", str(e))

    def join_game_async(self):
        def worker():
            try:
                self.game_info = self.client.servant.joinGame(self.player_info.username)
                if not self.left_lobby:
                    self.open_game_window_if_ready()
            except Boggled.LobbyException:
                if not self.left_lobby:
                    messagebox.showerror("Lobby Insufficient Players", "Lobby Insufficient Players")
                    self.open_main_menu_if_not_opened()
            except Exception as e:
                if not self.left_lobby:
                    messagebox.showerror("Error", str(e))

        threading.Thread(target=worker).start()

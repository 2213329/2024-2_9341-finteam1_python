from pathlib import Path
from tkinter import Tk, Canvas, Button, PhotoImage

class MainMenu:
    def __init__(self, root, client):
        self.root = root
        self.client = client
        self.root.geometry("1077x622")
        self.root.configure(bg="#FFFFFF")

        self.canvas = Canvas(
            self.root,
            bg="#FFFFFF",
            height=622,
            width=1077,
            bd=0,
            highlightthickness=0,
            relief="ridge"
        )
        self.canvas.place(x=0, y=0)

        self.setup_ui()

    def relative_to_assets(self, path: str) -> Path:
        OUTPUT_PATH = Path(__file__).parent
        ASSETS_PATH = OUTPUT_PATH / Path(r"assets/mainMenu")
        return ASSETS_PATH / Path(path)

    def setup_ui(self):
        image_image_1 = PhotoImage(file=self.relative_to_assets("image_1.png"))
        self.canvas.create_image(540.0, 311.0, image=image_image_1)

        button_image_1 = PhotoImage(file=self.relative_to_assets("button_1.png"))
        self.button_1 = Button(
            image=button_image_1,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_view_leaderboard,
            relief="flat"
        )
        self.button_1.place(x=423.194, y=443.274, width=222.110, height=46.650)

        button_image_2 = PhotoImage(file=self.relative_to_assets("button_2.png"))
        self.button_2 = Button(
            image=button_image_2,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_start_game,
            relief="flat"
        )
        self.button_2.place(x=423.194, y=389.137, width=216.966, height=46.650)

        image_image_2 = PhotoImage(file=self.relative_to_assets("image_2.png"))
        self.canvas.create_image(534.0, 232.0, image=image_image_2)

        # Keep a reference to the images to prevent garbage collection
        self.images = [image_image_1, button_image_1, button_image_2, image_image_2]

    def handle_start_game(self):
        # Code to start the game
        self.client.handle_start_game()

    def handle_view_leaderboard(self):
        self.client.handle_view_leaderboard()

if __name__ == "__main__":
    window = Tk()
    client = None  # Replace with actual Client instance if needed
    main_menu = MainMenu(window, client)
    window.mainloop()

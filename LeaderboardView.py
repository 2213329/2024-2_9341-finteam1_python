from pathlib import Path
from tkinter import Canvas, Button, PhotoImage, Frame, Scrollbar
from tkinter import ttk

class LeaderboardView:
    def __init__(self, root, client, leaderboard, player_info):
        self.client = client
        self.leaderboard = leaderboard
        self.player_info = player_info

        self.root = root
        self.root.geometry("1077x650")
        self.root.configure(bg="#FFFFFF")
        self.root.title("Leaderboard")

        self.canvas = Canvas(
            self.root,
            bg="#FFFFFF",
            height=650,
            width=1077,
            bd=0,
            highlightthickness=0,
            relief="ridge"
        )
        self.canvas.place(x=0, y=0)

        self.setup_ui()

    def relative_to_assets(self, path: str) -> Path:
        OUTPUT_PATH = Path(__file__).parent
        ASSETS_PATH = OUTPUT_PATH / Path(r"assets/res")
        return ASSETS_PATH / Path(path)

    def setup_ui(self):
        self.background_image = PhotoImage(file=self.relative_to_assets("LeaderboardsContent.png"))
        self.canvas.create_image(538.5, 325.0, image=self.background_image)

        # Create a frame for the leaderboard table
        table_frame = Frame(self.root, bg="#FFFFFF")
        table_frame.place(relx=0.15, rely=0.25, relwidth=0.7, relheight=0.5)

        # Create a Treeview to display leaderboard data
        columns = ("Rank", "Username", "Highest Score")
        self.leaderboard_table = ttk.Treeview(table_frame, columns=columns, show="headings")
        self.leaderboard_table.heading("Rank", text="Rank")
        self.leaderboard_table.heading("Username", text="Username")
        self.leaderboard_table.heading("Highest Score", text="Highest Score")

        self.leaderboard_table.column("Rank", anchor="center", width=100)
        self.leaderboard_table.column("Username", anchor="center", width=200)
        self.leaderboard_table.column("Highest Score", anchor="center", width=200)

        # Insert leaderboard data
        for i, player in enumerate(self.leaderboard.playerList):
            self.leaderboard_table.insert("", "end", values=(i + 1, player.username, player.highestScore))

        # Create a scrollbar for the table
        scrollbar = Scrollbar(table_frame, orient="vertical", command=self.leaderboard_table.yview)
        self.leaderboard_table.configure(yscroll=scrollbar.set)
        scrollbar.pack(side="right", fill="y")
        self.leaderboard_table.pack(fill="both", expand=True)

        # Create a back button
        back_button = Button(
            self.root,
            text="Back",
            command=self.on_back,
            bg="#FF5656",
            fg="white",
            borderwidth=0,
            highlightthickness=0
        )
        back_button.place(relx=0.1, rely=0.85, width=100, height=30)

        # Keep a reference to the images to prevent garbage collection
        self.images = [self.background_image]

    def on_back(self):
        # Show the main menu
        self.client.show_main_menu()

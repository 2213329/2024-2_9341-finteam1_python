import time
import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import threading
import Boggled

class GameWindow:
    def __init__(self, root, client, player_info, game_info):
        self.root = root
        self.client = client
        self.player_info = player_info
        self.game_info = game_info
        self.current_letters = ""
        self.polling_active = False
        self.polling_thread = None
        self.round_winner_displayed = False  # Flag to track round winner popup display
        self.game_end_displayed = False
        self.init_ui()
        self.start_game()
        self.start_polling_game_data()

    def relative_to_assets(self, path: str) -> str:
        ASSETS_PATH = 'assets/gameWindow'
        return f"{ASSETS_PATH}/{path}"

    def init_ui(self):
        self.root.title("Game Window")
        self.root.geometry("1077x622")
        self.root.resizable(False, False)

        try:
            background_image = Image.open(self.relative_to_assets("GameWindow.png"))
            background_photo = ImageTk.PhotoImage(background_image)
            self.background_label = tk.Label(self.root, image=background_photo)
            self.background_label.image = background_photo  # keep a reference!
            self.background_label.place(x=0, y=0, relwidth=1, relheight=1)

            self.letter_grid = [[None for _ in range(5)] for _ in range(4)]
            for row in range(4):
                for col in range(5):
                    cell = tk.Label(self.background_label, text="", font=("Arial", 24, "bold"), fg="black")
                    cell.place(x=250 + col * 90, y=80 + row * 90, width=90, height=90)
                    self.letter_grid[row][col] = cell

            self.input_box = tk.Entry(self.background_label, font=("Arial", 18, "bold"), fg="black")
            self.input_box.place(x=(1077 - 500) // 2, y=622 - 120, width=500, height=70)
            self.input_box.bind("<Return>", lambda event: self.handle_submit_word())

            self.status_label = self.create_label(self.background_label, "Game Status: Before Round", 20, 80, 200, 30, 10, "white")
            self.timer_label = self.create_label(self.background_label, "1", 700, 25, 60, 30, 28, "white")
            self.score_label = self.create_label(self.background_label, "Score: 0", 850, 40, 200, 30, 21, "white")
            self.round_label = self.create_label(self.background_label, "1", 560, 25, 30, 30, 28, "white")
            self.player_scores_label = self.create_label(self.background_label, "Player Game Scores: ", 720, 100, 200, 150, 14, "white")

            self.submitted_words = []  # Initialize as a list
            self.submitted_words_model = tk.StringVar(value=self.submitted_words)
            self.submitted_words_list = tk.Listbox(self.background_label, listvariable=self.submitted_words_model, font=("Arial", 14), fg="black")
            self.submitted_words_list.place(x=900, y=80, width=150, height=400)

            if self.game_info.currentRound > 0:
                self.current_letters = self.game_info.roundInfoVar[self.game_info.currentRound - 1].randomLetterList
                self.load_letters_into_grid(self.current_letters)

        except Exception as e:
            print(e)
            messagebox.showerror("Initialization Error", str(e))

    def create_label(self, parent, text, x, y, width, height, font_size, bg_color):
        label = tk.Label(parent, text=text, font=("Arial", font_size, "bold"), fg="black", bg=bg_color)
        label.place(x=x, y=y, width=width, height=height)
        return label

    def load_letters_into_grid(self, letters):
        for i in range(len(letters)):
            row = i // 5
            col = i % 5
            self.letter_grid[row][col].config(text=letters[i])

    def handle_submit_word(self):
        word = self.input_box.get()
        if self.game_info.status != Boggled.ONGOING_ROUND:
            messagebox.showerror("Invalid Action", "Cannot submit words outside of an ongoing round")
            return

        if len(word) < 4:
            messagebox.showerror("Invalid Word", "Word must be at least 4 letters long")
            return

        if not self.is_word_in_grid(word):
            messagebox.showerror("Invalid Word", "Word must be composed of the letters in the grid")
            return

        try:
            self.client.servant.submitWord(self.player_info.username, self.game_info.gameId, word)
            self.submitted_words.append(word)  # Append to the list
            self.submitted_words_model.set(self.submitted_words)  # Update the StringVar
        except Exception as e:
            messagebox.showerror("Error", str(e))

    def is_word_in_grid(self, word):
        letter_counts = [0] * 26
        for letter in self.current_letters.lower():
            if 'a' <= letter <= 'z':
                letter_counts[ord(letter) - ord('a')] += 1

        for letter in word.lower():
            if 'a' <= letter <= 'z':
                index = ord(letter) - ord('a')
                letter_counts[index] -= 1
                if letter_counts[index] < 0:
                    return False
        return True

    def close_program(self):
        self.root.destroy()

    def handle_leave_game(self):
        self.stop_polling_game_data()  # Stop polling before destroying windows
        try:
            self.client.servant.leaveGame(self.player_info.username)
        except Exception as e:
            messagebox.showerror("Error", str(e))
        self.close_program()

    def show_main_menu(self):
        self.client.show_main_menu()

    def start_game(self):
        self.update_game_status(Boggled.BEFORE_ROUND)
        self.start_new_round()

    def start_new_round(self):
        self.round_winner_displayed = False  # Reset flag for new round
        self.update_game_status(Boggled.ONGOING_ROUND)

    def check_for_game_end(self):
        winner = None
        for player in self.game_info.players:
            if player.currentGameScore >= 3:
                winner = player
                break

        if winner or self.game_info.status == Boggled.ENDED:
            self.polling_active = False
            winner_message = "Game Over!\n\nGame Wins:\n"
            for player in self.game_info.players:
                winner_message += f"{player.username}: {player.wins} wins\n"
            if winner:
                winner_message += f"\nPlayer with 3 points: {winner.username}"
            self.show_game_end_popup(winner_message)

    def show_game_end_popup(self, message):
        if not self.game_end_displayed:
            self.game_end_displayed = True
            game_end_dialog = tk.Toplevel()
            game_end_dialog.title("Game Over")
            tk.Label(game_end_dialog, text=message, justify=tk.LEFT, padx=20, pady=20, fg="black").pack()
            tk.Button(game_end_dialog, text="Leave",
                      command=lambda: [game_end_dialog.destroy(), self.handle_leave_game()]).pack(pady=10)
            game_end_dialog.transient(self.root)
            game_end_dialog.grab_set()


    def update_game_status(self, status):
        self.game_info.status = status
        status_name = ""
        status = self.game_info.status
        if status == Boggled.BEFORE_ROUND:
            status_name = "Before Round"
            if self.round_winner_displayed:
                self.round_winner_displayed = False
        elif status == Boggled.ONGOING_ROUND:
            status_name = "Ongoing Round"
        elif status == Boggled.AFTER_ROUND:
            status_name = "After Round"
            if not self.round_winner_displayed:
                self.display_round_winner()
        elif status == Boggled.ENDED:
            status_name = "Ended"
        if status == Boggled.BEFORE_ROUND and self.game_info.currentRound > 0:
            self.current_letters = self.game_info.roundInfoVar[self.game_info.currentRound - 1].randomLetterList
            self.load_letters_into_grid(self.current_letters)
            self.submitted_words = []  # Clear the list
            self.submitted_words_model.set(self.submitted_words)  # Update the StringVar
        self.status_label.config(text=f"Game Status: {status_name}")

    def update_timer_label(self):
        self.timer_label.config(text=f"{self.game_info.currentRoundDuration}s")

    def update_score_label(self):
        current_score = self.calculate_current_score()
        self.score_label.config(text=f"Score: {current_score}")

    def update_round_label(self):
        self.round_label.config(text=str(self.game_info.currentRound))

    def update_player_scores_label(self):
        scores_text = "Player Scores:\n"
        for player in self.game_info.players:
            scores_text += f"{player.username}: {player.currentGameScore}\n"
        self.player_scores_label.config(text=scores_text)

    def calculate_current_score(self):
        score = 0
        current_round = self.game_info.roundInfoVar[self.game_info.currentRound - 1]
        for word_submission in current_round.wordSubmissions:
            if word_submission.username == self.player_info.username:
                score += len(word_submission.word)
        return score

    def display_round_winner(self):
        if not self.round_winner_displayed:
            self.round_winner_displayed = True  # Set flag to prevent multiple popups

            round_winner = self.game_info.roundInfoVar[self.game_info.currentRound - 1].roundWinner
            message = f"Winner of Round {self.game_info.currentRound}: {round_winner}"
            messagebox.showinfo("Round Winner", message)

            # Update player scores in the UI
            self.update_player_scores_label()

    def start_polling_game_data(self):
        if not self.polling_active:
            self.polling_active = True
            self.polling_thread = threading.Thread(target=self.poll_game_data)
            self.polling_thread.start()

    def poll_game_data(self):
        while self.polling_active:
            try:
                game_info = self.client.servant.getGameData(self.player_info.username, self.game_info.gameId)
                if game_info:
                    self.game_info = game_info
                    self.update_game_status(self.game_info.status)
                    self.update_timer_label()
                    self.update_score_label()
                    self.update_round_label()
                    self.update_player_scores_label()
                    self.check_for_game_end()  # Check for game end condition
            except Exception as e:
                print("Error polling game data:", e)
            time.sleep(0.5)

    def stop_polling_game_data(self):
        self.polling_active = False
        if self.polling_thread:
            self.polling_thread.join()
            self.polling_thread = None
